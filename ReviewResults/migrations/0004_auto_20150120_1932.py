# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ReviewResults', '0003_auto_20150120_1913'),
    ]

    operations = [
        migrations.RenameField(
            model_name='review_rule_property',
            old_name='rule_id',
            new_name='rule',
        ),
    ]
