# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ReviewResults', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='review_rule',
            old_name='rule_master_id',
            new_name='rule_master',
        ),
    ]
