# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='review_rule',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rule_object', models.CharField(max_length=25)),
                ('rule_name', models.CharField(max_length=100)),
                ('rule_desc', models.CharField(max_length=100)),
                ('rule_error', models.CharField(max_length=100)),
                ('rule_custom_flag', models.CharField(default=b'N', max_length=1)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='review_rule_category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rule_category', models.CharField(max_length=25)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='review_rule_group',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rule_desc', models.CharField(max_length=100)),
                ('rule_category', models.CharField(max_length=25)),
                ('rule_priority', models.CharField(max_length=25)),
                ('rule_operation', models.CharField(max_length=50, null=True)),
                ('rule_property', models.CharField(max_length=50, null=True)),
                ('rule', models.ForeignKey(to='ReviewResults.review_rule')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='review_rule_group_name',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rule_group_name', models.CharField(max_length=25)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='review_rule_master',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rule_master_object', models.CharField(max_length=25)),
                ('rule_master_desc', models.CharField(max_length=100)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='review_rule_operation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rule_operation', models.CharField(max_length=50)),
                ('rule_id', models.ForeignKey(to='ReviewResults.review_rule')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='review_rule_priority',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rule_priority', models.CharField(max_length=25)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='review_rule_property',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rule_property', models.CharField(max_length=50)),
                ('rule_id', models.ForeignKey(to='ReviewResults.review_rule')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='review_rule_group',
            name='rule_group_name',
            field=models.ForeignKey(to='ReviewResults.review_rule_group_name'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='review_rule',
            name='rule_master_id',
            field=models.ForeignKey(to='ReviewResults.review_rule_master'),
            preserve_default=True,
        ),
    ]
