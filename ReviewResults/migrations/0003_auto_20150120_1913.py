# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ReviewResults', '0002_auto_20150119_1913'),
    ]

    operations = [
        migrations.RenameField(
            model_name='review_rule_operation',
            old_name='rule_id',
            new_name='rule',
        ),
    ]
