from django.db import models


class review_rule_master(models.Model):
    rule_master_object = models.CharField(max_length = 25)
    rule_master_desc = models.CharField(max_length = 100)

    def __unicode__(self):
        return self.rule_master_desc

class review_rule(models.Model):
    rule_master = models.ForeignKey(review_rule_master)
    rule_object = models.CharField(max_length = 25)
    rule_name = models.CharField(max_length = 100)
    rule_desc = models.CharField(max_length = 100)
    rule_error = models.CharField(max_length = 100)
    rule_custom_flag = models.CharField(max_length = 1, default = 'N')

    def __unicode__(self):
        return self.rule_name

class review_rule_operation(models.Model):
    rule = models.ForeignKey(review_rule)
    rule_operation = models.CharField(max_length = 50)

    def __unicode__(self):
        return self.rule_operation

class review_rule_property(models.Model):
    rule = models.ForeignKey(review_rule)
    rule_property = models.CharField(max_length = 50)

    def __unicode__(self):
        return self.rule_property

class review_rule_category(models.Model):
    rule_category = models.CharField(max_length = 25)

    def __unicode__(self):
        return self.rule_category

class review_rule_priority(models.Model):
    rule_priority = models.CharField(max_length = 25)

    def __unicode__(self):
        return self.rule_priority

class review_rule_group_name(models.Model):
    rule_group_name = models.CharField(max_length=25)

    def __unicode__(self):
        return self.rule_group_name

class review_rule_group(models.Model):
    rule_group_name = models.ForeignKey(review_rule_group_name)
    rule = models.ForeignKey(review_rule)
    rule_desc = models.CharField(max_length = 100)
    rule_category = models.CharField(max_length = 25)
    rule_priority = models.CharField(max_length = 25)
    rule_operation = models.CharField(max_length = 50, null = True)
    rule_property = models.CharField(max_length = 50, null = True)

    def __unicode__(self):
        return "%s %s" % (self.rule_group_name.rule_group_name, self.rule.rule_name)

