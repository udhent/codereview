from django.shortcuts import render, render_to_response
from .models import review_rule_group_name, review_rule_master, review_rule_group, review_rule_operation, review_rule_property, review_rule, review_rule_priority, review_rule_category
from django.template.context import RequestContext
from django.http import HttpResponse
from django.core.exceptions import PermissionDenied
import json

# Create your views here.
def home(request):
    _rule_group = review_rule_group_name.objects.all()
    return render(request, "ReviewResults/home.html", {"rule_group": _rule_group})


def get_or_create_group(request):
    if request.is_ajax():
        if request.method == "POST":
            response_data = {}
            rule_group_name = request.POST.get("rule_group_name")
            if rule_group_name == "":
                response_data["status"] = "not allowed"
                response_data["reason"] = "Empty Value"
                return HttpResponse(json.dumps(response_data), content_type="application/json")
            try:
                group = review_rule_group_name.objects.get(rule_group_name=rule_group_name)
                group_id = group.id
            except:
                group = review_rule_group_name(rule_group_name=rule_group_name)
                group.save()
                group_id = group.id
            response_data["status"] = "success"
            response_data["id"] = group_id
            return HttpResponse(json.dumps(response_data), content_type="application/json")
    else:
        raise PermissionDenied


def review_step_2(request):
    if request.is_ajax():
        if request.method == "GET":
            id = request.GET.get("id")
            group = review_rule_group_name.objects.get(id=id)
            rule_group = review_rule_group.objects.filter(rule_group_name=group)
            all_rules = review_rule.objects.all()
            selected_rules = []
            for rules in rule_group:
                if rules.rule not in selected_rules:
                    selected_rules.append(rules.rule)

            template = "ReviewResults/step_2.html"
            data = {
                "group": group,
                "rule_master": rule_master,
                "selected_rules": selected_rules,
                "rule_group": rule_group
            }
            return render_to_response(template, data, context_instance=RequestContext(request))
    else:
        raise PermissionDenied


def review_step_3(request):
    if request.is_ajax():
        response_data = {}
        if request.method == "POST":
            group_name_id = request.POST.get("group_name_id")
            rule_masters_arr = request.POST.getlist("rule_masters[]")
            rule_group_name = review_rule_group_name.objects.get(id=group_name_id)
            #rg = review_rule_group.objects.filter(rule_group_name=rule_group_name)
            if u"" not in rule_masters_arr:
                # convert post data to int
                s = []
                for y in rule_masters_arr:
                    s.append(int(y))
                rule_masters_arr = s
                for rule_master_id in rule_masters_arr:
                    rm = review_rule_master.objects.get(id=rule_master_id)
                    rules = rm.review_rule_set.all()
                    for rule in rules:
                        exists = review_rule_group.objects.filter(rule_group_name=rule_group_name, rule=rule)
                        if len(exists) == 0:
                            rg = review_rule_group(rule_group_name=rule_group_name, rule=rule)
                            rg.save()

                current_master = []
                rg = review_rule_group.objects.filter(rule_group_name=rule_group_name)
                for rule in rg:
                    if rule.rule.rule_master.id not in current_master:
                        current_master.append(rule.rule.rule_master.id)

                if rule_masters_arr != current_master:
                    for id in current_master:
                        if id not in rule_masters_arr:
                            rm = review_rule_master.objects.get(id=id)
                            rules = review_rule.objects.filter(rule_master=rm)
                            rg = review_rule_group.objects.filter(rule_group_name=rule_group_name, rule=rules)
                            rg.delete()


            # existing_rule_group = review_rule_group.objects.filter(rule_group_name=rule_group_name)
                response_data["status"] = "success"
            # response_data["rule"] = existing_rule_group
            else:
                try:
                    rg = review_rule_group.objects.filter(rule_group_name=rule_group_name)
                    rg.delete()
                    response["status"] = "Master Rule deleted"
                except:
                    response_data["status"] = "failed"
            return HttpResponse(json.dumps(response_data), content_type="application/json")
    else:
        raise PermissionDenied


def load_step_3(request):
    if request.is_ajax():
        if request.method == "GET":
            group_name_id = request.GET.get("id")
            group_name = review_rule_group_name.objects.get(id=group_name_id)
            rule_group_rule = review_rule_group.objects.filter(rule_group_name=group_name)
            # print "tes"
            template = "ReviewResults/step_3.html"
            data = {"rule_groups": rule_group_rule, "group_name": group_name}
        return render_to_response(template, data, context_instance=RequestContext(request))
    else:
        raise PermissionDenied


def save_step_3(request):
    if request.is_ajax():
        response_data = {}
        if request.method == "POST":
            group_name_id = request.POST.get("group_name_id")
            operations = request.POST.getlist("operations[]")
            properties = request.POST.getlist("properties[]")
            rule_ids = request.POST.getlist("rule_id[]")
            group_name = review_rule_group_name.objects.get(id=group_name_id)
            for x in range(0, len(rule_ids)):
                rule = review_rule.objects.get(id=rule_ids[x])
                if operations[x] == u'':
                    operation = ""
                else:
                    try:
                        id = int(operations[x])
                        operation = review_rule_operation.objects.get(id=id)
                        operation = operation.rule_operation
                    except:
                        operation = operations[x]
                if properties[x] == u'':
                    propert = ""
                else:
                    try:
                        id = int(properties[x])
                        propert = review_rule_property.objects.get(id=id, rule=rule)
                        propert = propert.rule_property
                    except:
                        propert = properties[x]
                rule_group = review_rule_group.objects.get(rule_group_name=group_name, rule=rule)
                rule_group.rule_operation = operation
                rule_group.rule_property = propert
                rule_group.save()

            response_data["status"] = "success"
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    else:
        raise PermissionDenied


def load_step_4(request):
    if request.is_ajax():
        if request.method == "GET":
            id = request.GET.get("id")
            rg_name = review_rule_group_name.objects.get(id=id)
            rg  = review_rule_group.objects.filter(rule_group_name=rg_name)
            categories = review_rule_category.objects.all()
            priorities = review_rule_priority.objects.all()
            template = "ReviewResults/step_4.html"
            data = {
                "rule_groups": rg,
                "priorities": priorities,
                "categories": categories,
                "group_name": rg_name,
            }
            return render_to_response(template, data, context_instance=RequestContext(request))
    else:
        raise PermissionDenied


def save_step_4(request):
    if request.is_ajax():
        response_data = {}
        if request.method == "POST":
            id = request.POST.get("rule_group_name")
            group_name = review_rule_group_name.objects.get(id=int(id))
            rule_group = review_rule_group.objects.filter(rule_group_name=group_name)
            categories = request.POST.getlist("rule_categories[]")
            priorities = request.POST.getlist("rule_priorities[]")
            rule_ids = request.POST.getlist("rule_ids[]")


            for x in range(0, len(rule_ids)):
                rule = review_rule.objects.get(id=int(rule_ids[x]))
                # print rule
                try:
                    group = review_rule_group.objects.get(rule_group_name=group_name, rule=rule)
                    # print group

                    if categories[x] == u"":
                        rule_category = ""
                    else:
                        category_id = int(categories[x])
                        category = review_rule_category.objects.get(id=category_id)
                        rule_category = category.rule_category

                    if priorities[x] != u"":
                        priority_id = int(priorities[x])
                        priority = review_rule_priority.objects.get(id=priority_id)
                        rule_priority = priority.rule_priority
                    else:
                        rule_priority = ""


                    group.rule_category = rule_category
                    group.rule_priority = rule_priority
                    group.rule_desc = rule.rule_desc + " " + group.rule_operation + " " + group.rule_property
                    group.save()
                    response_data["status"] = "success"
                except:
                    response_data["status"] = "failed"
            print response_data
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    else:
        raise PermissionDenied

def first_step(request):
    pass