from django.conf.urls import patterns, include, url
from django.contrib import admin
from .views import home, get_or_create_group, review_step_2, review_step_3, load_step_3, save_step_3, load_step_4, save_step_4


urlpatterns = patterns('',
    url(r'^$', home, name='home'),
    url(r'^get-or-create-group/$', get_or_create_group, name="get_or_create_group"),
    url(r'^review-step-2/$', review_step_2, name="review_step_2"),
    url(r'^review-step-3/$', review_step_3, name="review_step_3"),
    url(r'^load-step-3-data/$', load_step_3, name="load_step_3"),
    url(r'^save-step-3/$', save_step_3, name="save_step_3"),
    url(r'^load-step-4/$', load_step_4, name="load_step_4"),
    url(r'^save-step-4/$', save_step_4, name="save_step_4"),
)
